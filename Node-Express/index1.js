const bodyParser = require('body-parser');
const express = require('express');
const http = require('http');
const hostname = 'localhost';
const port = 3000;

const morgan =require('morgan');
const app = express();

function reversestr(str)
{
        let j= str.length;
        console.log(j);
        for (let i=2;i<str.length/2;i++){
                console.log('here');
                let temp =str[i];
                 str[i]=str[j-i-1];
                 str[j-i-1]=temp;

        }
        console.log(str);
}
reversestr('Raghav');

app.use(morgan('dev'));
app.use(express.static(__dirname+'/Public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
        extended: true
      }));

app.all('/dishes',(req,res,next)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','text/plain');
        next();                 //vimp , req, res passed to next request
});

app.get('/dishes',(req,res,next)=>{
        res.end('Will send all the dishes to you!' );
});

app.post('/dishes',(req,res,next)=>{
        res.end('Will add the dish :'+ req.body.name 
        +' With details : ' +req.body.description);
});

app.put('/dishes',(req,res,next)=>{
        res.statusCode=403;
        res.end('PUT operation not supported on dishes');
        
});

app.delete('/dishes',(req,res,next)=>{
        res.end('Deleting all dishes' );
});


app.get('/dishes/:dishId/:price',(req,res,next)=>{
        res.end('Will send you dish details :' + req.params.price );
});

app.post('/dishes/:dishId',(req,res,next)=>{
        res.statusCode=403;
        res.end('POST operation not supported on specific dish');
});

app.put('/dishes/:dishId',(req,res,next)=>{
        res.write('Updating the dish :' + req.params.dishId);
        res.end('\nWill Update the dish: ' + req.body.name + ' with details :' 
        + req.body.description);
        
});

app.delete('/dishes/:dishId',(req,res,next)=>{
        res.end('Deleting specific dish' );
});


app.use((req,res,next) => {
   //console.log(req.headers);
    res.statusCode=200;
    res.setHeader('Content-Type','text/html');
    res.end('<html><body><h1>This is an Express Server</h1></body></html>');
});

const server = http.createServer(app);

server.listen(port,hostname,()=>{
        console.log(`server running at http:\\${hostname}:${port}`);

});