const bodyParser = require('body-parser');
const express = require('express');
const http = require('http');
const hostname = 'localhost';
const port = 3000;

const morgan =require('morgan');
const app = express();

const dishRouter = require('./Routes/dishRouter');

app.use(morgan('dev'));
app.use(express.static(__dirname+'/Public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
        extended: true
      }));

app.use('/dishes',dishRouter);


app.get('/dishes/:dishId/:price',(req,res,next)=>{
        res.end('Will send you dish details :' + req.params.price );
});

app.post('/dishes/:dishId',(req,res,next)=>{
        res.statusCode=403;
        res.end('POST operation not supported on specific dish');
});

app.put('/dishes/:dishId',(req,res,next)=>{
        res.write('Updating the dish :' + req.params.dishId);
        res.end('\nWill Update the dish: ' + req.body.name + ' with details :' 
        + req.body.description);
        
});

app.delete('/dishes/:dishId',(req,res,next)=>{
        res.end('Deleting specific dish' );
});


app.use((req,res,next) => {
   //console.log(req.headers);
    res.statusCode=200;
    res.setHeader('Content-Type','text/html');
    res.end('<html><body><h1>This is an Express Server</h1></body></html>');
});

const server = http.createServer(app);

server.listen(port,hostname,()=>{
        console.log(`server running at http:\\${hostname}:${port}`);

});