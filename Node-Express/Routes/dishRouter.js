const express = require('express');
const bodyParser=require('body-parser');



const dishRouter = express.Router();

dishRouter.use(bodyParser.json());

dishRouter.route('/:dishId')

.all((req,res,next)=>{
    res.statusCode=200;
    res.setHeader('Content-Type','text/plain');
    next();                 //vimp , req, res passed to next request
})

.get((req,res,next)=>{
    res.end('Routed \n Will send all the dishes to you!' );
})

.post((req,res,next)=>{
    res.end('Routed \n Will add the dish :'+ req.body.name 
    +' With details : ' +req.body.description);
})

.put((req,res,next)=>{
    res.statusCode=403;
    res.end('Routed \n PUT operation not supported on dishes');
    
})

.delete((req,res,next)=>{
    res.end('Routed \n Deleting all dishes' );
});

dishRouter.route('/').all((req,res,next)=>{
    console.log('Routed dishes endpoint');
    res.setHeader('Content-Type','text/plain');
    res.end('\nRouted dishes endpoint');
})
module.exports = dishRouter;
