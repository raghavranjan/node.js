const express = require('express');
const bodyParser=require('body-parser');
const mongoose = require('mongoose');
const Dishes = require('../Models/dishmodel1');


const dishRouter = express.Router();

dishRouter.use(bodyParser.json());

dishRouter.route('/')
.get((req,res,next)=>{
    //res.end('Routed \n Will send all the dishes to you!' );
    Dishes.find({})
    .then((dishList)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(dishList);
    }, (err) => {
        next(err)
    }).catch((err)=>{
        console.log('error1');
        var err = new Error('You are not Authenticated!');
      res.setHeader('WWW-Authenticate','Basic');
      err.status=401;
    })
})

.post((req,res,next)=>{
    //res.end('Routed \n Will add the dish :'+ req.body.name 
    // +' With details : ' +req.body.description);
    Dishes.create(req.body).then((dish)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(dish);
        console.log('Dish created',dish);

    })
})

.put((req,res,next)=>{
    res.statusCode=403;
    res.end('Routed \n PUT operation not supported on dishes');
    
})

.delete((req,res,next)=>{
    res.end('Deleting Dishes');
   Dishes.remove({}).then((resp)=>{
    res.statusCode=200;
    res.setHeader('Content-Type','application/json');
    res.json(resp);
    res.end('\nDeleted All Dishes')
   }).catch((err)=>{
       next(err);
   })
});

dishRouter.route('/:dishId')
.get((req,res,next)=>{
    Dishes.findById(req.params.dishId).then((dish)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(dish);
    })
.catch((err)=>{
    next(err);})
})

.post((req,res,next)=>{
    res.statusCode=403;
    res.end('\nPOST NOT SUPPORTED');
    
})
.put((req,res,next)=>{
    res.statusCode=200;
    res.setHeader('Content-Type','application/json');
    Dishes.findByIdAndUpdate(req.params.dishId,{$set : req.body},{new : true})
    .then((dish)=>{
        res.json(dish)
    })

})
.delete((req,res,next)=>{
    res.end('Deleting');
    Dishes.findByIdAndRemove(req.params.dishId).then(resp)
    {
        res.statusCode=200;
    res.setHeader('Content-Type','application/json');
    res.json(resp);
    res.end('\nDeleted Successfully');   
    }
})


module.exports = dishRouter;
