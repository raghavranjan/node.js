var express = require('express');
var bodyParser = require('body-parser');
var UserSchema = require('./../Models/user');

var router = express.Router();
router.use(bodyParser.json());

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
  console.log('In USERS ENDPOINT');
});
/*------------------------------SIGN UP------------------------------------------------------*/
router.post('/signup', (req,res,next)=>{
  console.log(req.body);
  res.end(req.body);
  if(req.session.user){
    var err = new Error('User already logged in');
      err.status = 403;
      return next(err);
  }
  UserSchema.findOne({username : req.body.username})
  .then((user)=>{
    if(user != null){
      var err = new Error('User' + req.body.username + ' already exists');
      err.status = 403;
      next(err);
    }
    else{
      return UserSchema.create({
        username : req.body.username,
        password : req.body.password
      });
    }

  }).then((user)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type','application/json');
    res.json({status:'Registration Successful', User: user})
  },(err)=>next(err))
  .catch((err)=>{next(err);})
  
});

/*-----------------------------------LOGIN-----------------------------------------*/
router.post('/login',(req,res,next)=>{
  console.log('welcome to login page\n');

  if(!req.session.user){
    console.log('not already logged in\n');
    var authHeader = req.headers.authorization;
    
   
    if (!authHeader)
    {
      console.log('No Login header')
      var err = new Error('You are not Authenticated!');
      res.setHeader('WWW-Authenticate','Basic');
      err.status=401;
      return next(err);
    }
    console.log('Need to login')
    var auth = new Buffer.from(authHeader.split(' ')[1],'base64').toString().split(':');
    var username = auth[0];
    var password = auth[1];
    
    UserSchema.findOne({username : username}).then((user)=>{
      console.log('Trying to login for ', user);
      if(user == null){

        var err = new Error('User Doesnot Exists');
        res.setHeader('WWW-Authenticate','Basic');
        err.status=403;
        return next(err);
      }
      else if(user.password !== password)
      {
        var err = new Error('Incorrect Password');
        res.setHeader('WWW-Authenticate','Basic');
        err.status=403;
        return next(err);
      }
      else if (user.username === username && user.password === password){
        req.session.user = 'authenticated';
        res.statusCode=200;
        res.setHeader('Content-Type','text/plain');
        res.end('Your are Authenticated');
        next();
      }
      
    })
    .catch((err)=>{
      next(err);})
  }
  else
  {
    res.statusCode= 200;
    res.setHeader('Content-Type','text/plain');
    res.end('You are already Authenticated');
  }
});

router.get('/logout',(req,res)=>{
  if(req.session){
    req.session.destroy();
    res.clearCookie('session-id');
    res.redirect('/');
  }
  else{
    var err = new Error('Your are not logged in!');
    err.status = 403;
    next(err);
  }
});

module.exports = router;
