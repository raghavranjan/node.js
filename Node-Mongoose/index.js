console.log('Running--');
const mongoose = require('mongoose');
const Dishes = require('./Models/dishmodel');

const url = 'mongodb://localhost:27017/conFusion';
const connect = mongoose.connect(url);



console.log('Running');

connect.then((db)=> {
    console.log('-------CONNECTED TO MONGODB----------');
    var newDish = Dishes({
        name : 'Pizza2',
        description : 'Italian'
    });
    newDish.save()
    .then((dish)=>{
        console.log('----INSERTED------')
        console.log(dish);
        return Dishes.find({});
    })
    .then((dishList) => {
        
        console.log(dishList);

        return mongoose.connection.close();
    }).catch((err)=>{
        console.log('catch 2\n')
        console.log(err)
    }).finally(()=>{
        mongoose.connection.close();
        console.log('Program ENDS');
    })
}).catch((err)=>{
    console.log('---------connection error------');
});
