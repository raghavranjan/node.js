console.log('Running--');
const mongoose = require('mongoose');
const DishDB = require('./Models/dishmodel1');

const url = 'mongodb://localhost:27017/conFusion';
const connect = mongoose.connect(url);



console.log('Running');
mongoose.set('useFindAndModify', false);
connect.then((db)=> {
    console.log('-------CONNECTED TO MONGODB----------');
    DishDB.create({
        namee : 'piiizzrrrzza',
        description : 'Italian23'
    })
    .then((dish) => {
        console.log('----INSERTED------')
        console.log(dish,'\n Updating for :',dish._id);
        dish.comments.push({
            rating : 4,
            comment : 'Very Nice1123',
            author : 'Raghav'})
        console.log('Updating Comments');
        return dish.save(); 
    }).then((dish) => {
        return DishDB.findByIdAndUpdate(dish._id,{
        $set : {
             description : 'Updated Pizza'}
        },
         {
             new : true 
        }
    ) })
    .then((dish) => {
        dish.comments.push({
            rating : 3,
            comment : 'Very Nice123',
            author : 'Raghav'})
        return dish.save() 
    })
    .then((dish) => {
        console.log(dish);
        DishDB.remove({});
        return mongoose.connection.close(); 

    }).catch((err)=>{
        console.log('catch 2\n')
        console.log(err)
    }).finally(()=>{
        mongoose.connection.close();
        console.log('Program ENDS');
    })
})
