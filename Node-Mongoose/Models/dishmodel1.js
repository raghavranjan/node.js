const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var commentSchema = new Schema({
    rating:  {
        type: Number,
        min: 1,
        max: 5,
        required: true
    },
    comment:  {
        type: String,
        required: true
    },
    author:  {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

var dishSchema = new Schema({
    namee: {
        type: String,
        required: true
        
    },
    description: {
        type: String,
        required: true
    },
    comments:[commentSchema],
    size : [Number]
}, {
    timestamps: true
});

var DishDB = mongoose.model('Disher', dishSchema);
module.exports = DishDB;