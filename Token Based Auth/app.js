var createError = require('http-errors');

var express = require('express');
var path = require('path');
//var cookieParser = require('cookie-parser');
var logger = require('morgan');

var session = require('express-session');
var FileStore = require('session-file-store')(session);

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const dishRouter = require('./routes/dishRouter');

var passport = require('passport');
var authenticate = require('./Authenticate');
/*---------------------------------SETTINNG UP MONGODB SERVER----------------------------*/

const mongoose = require('mongoose');
const Dishes = require('./Models/dishmodel1');
const url = 'mongodb://localhost:27017/conFusion';
const connect = mongoose.connect(url);

connect.then((db) => {
  console.log('---------CONNECTED TO MONGODB SERVER');
}, (err)=>{
  console.log('---Connection Error---',err);
  mongoose.connection.close();
});

/*-----------------------------------------------------------------------------------------*/

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//app.use(cookieParser('12345-67890-123311'));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);

/*-----------------------------ROUTING---------------------------------------------*/
app.use(session({
  name: 'session-id',
  secret: '123-123-123',
  saveUninitialized : false,
  resave : false,
  store : new FileStore()
}));

app.use(passport.initialize());
app.use(passport.session());


app.use('/users', usersRouter);

console.log('APP.JS RUNNING');


/*-----------------------------------AUTHENTICATION---------------------------------------*/


function auth(req,res,next)
{
  console.log(req.session);

  if(!req.user){
      var err = new Error('You are not Authenticated!');
      res.setHeader('WWW-Authenticate','Basic');
      err.status=401;
      return next(err);
  }
  else{
    next();
  }
}

app.use(auth);


/*-----------------------------------ROUTING----------------------------------------------*/

app.use('/dishes',dishRouter);
console.log('2');

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
  app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  console.log('error app.js');
  res.status(err.status || 500);
  res.setHeader('Content-Type','plain/text');
  res.end(err.message);

  /*res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error'); */
});

module.exports = app;
