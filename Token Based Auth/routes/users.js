var express = require('express');
var bodyParser = require('body-parser');
var UserSchema = require('../Models/userModel');
var passport = require('passport');
var Authenticate = require('../Authenticate');
var router = express.Router();
router.use(bodyParser.json());

/* GET users listing. */
router.get('/', function(req, res, next) {
  console.log(req.body);
  console.log(req.headers);  
  res.send('respond with a resource');
  console.log('In USERS ENDPOINT');
});

/*------------------------------SIGN UP------------------------------------------------------*/
router.post('/signup', (req,res,next)=>{
    console.log(req.body);
  UserSchema.register(new UserSchema ({username : req.body.username}) ,
  req.body.password, (err, user) => 
  {
    if(err){
        console.log(err , ' \n----' , req.body.username , req.body.password);
        res.statusCode = 403;
        res.setHeader('Content-Type','application/json');
        res.json({status: false, message : 'Registration Failed',err : err});
    }
    else{
            console.log('creating user');
            passport.authenticate('local')(req,res, ()=>{
            res.statusCode = 200;
            res.setHeader('Content-Type','application/json');
            res.end({success : true , message : 'registration successful ', user : user});
            console.log('\nuser created')
        });
    }

});

});


/*-----------------------------------LOGIN-----------------------------------------*/
router.post('/login', passport.authenticate('local'), (req,res,next)=>{
    if(err){
        console.log('Username password incorrect');
        var err = new Error('Username password incorrect');
        res.statusCode=401;
        res.setHeader('Content-Type','plain/text');
        res.end('Username password Incorrect');
        return next(err);
    }
            console.log('welcome to login page\n');
            res.statusCode = 200;
            res.setHeader('Content-Type','application/json');
            res.end({success : true , message : 'login successful '});

  
});

router.get('/logout',(req,res)=>{
  if(req.session){
    req.session.destroy();
    res.clearCookie('session-id');
    res.redirect('/');
  }
  else{
    var err = new Error('Your are not logged in!');
    err.status = 403;
    next(err);
  }
});

module.exports = router;
