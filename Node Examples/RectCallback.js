module.exports = (x,y,callback) => {
if(x<=0 || y<=0) {
    setTimeout(()=> callback(new Error('Invalid values'),null),2000);
}

else{
    setTimeout(()=> callback(null,
        {
            perimeter: (2*(x+y)),
            area:  (x*y),
            rvals(){console.log(this.perimeter,this.area)}
            
        }),
        2000);
        
}
console.log("This statement is after call to rect()");
console.log("THIS IS RECTANGLE CALLBACK JS");
}