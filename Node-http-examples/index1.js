const http = require('http');
const fs = require('fs');
const path = require('path');
const hostname = 'localhost';
const port = '3000';
const server = http.createServer((req,res)=> {
    console.log("req for "+req.url +" by method "+req.method);
    
    if (req.method == 'GET')
    {
        var fileUrl;
        
        if(req.url== '/')
        fileUrl='/index.html';

        else
        fileUrl=req.url;

        var filePath = path.resolve('./Public'+fileUrl);
        const fileExt = path.extname(filePath);
        console.log(fileExt);
        if(fileExt == '.html')
        {
            fs.stat(filePath,(err,stats)=>{
                console.log(stats);
                if(!stats){
                        res.statusCode=404;
                    res.setHeader=('Content-Type','text/html');
                    res.end('<html><body><p>Page not Found</p></body></html>');

                return;
            
                } 
                console.log('file exists');


                res.statusCode=200;
                res.setHeader('Content-Type','text/html');
                console.log('here 1');
                fs.createReadStream(filePath).pipe(res);
                console.log('here 2');
            });
        }
        else
        {
            res.statusCode=404;
            res.setHeader=('Content-Type','text/html');
            res.end('<html><title></title><body><p>ERROR 404: not ahtml file</p></body></html>');

        return;
        }
    }
    else {
        
        res.statusCode=404;
        res.setHeader=('Content-Type','text/html');
        res.end('<html><body><p><h1>ERROR 404 : REQ METHOD NOT SUPPORTED</h1></p></body></html>');

    return;
    }
})

                
server.listen(port, hostname, () => {
    console.log(`Server running at HTTP://${hostname}:${port}`);
});