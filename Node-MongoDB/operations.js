const assert = require('assert');


exports.insertDocument = (db,document,collection, callback) => {
    const coll=db.collection(collection);
    coll.insertOne(document, (err,result)=>{
        assert.strictEqual(err,null);
        console.log("--Inserted-- " , result.result.n , "docs " , collection);
        callback(result);
    });

};


exports.findDocuments = (db,collection, callback) => {
    const coll=db.collection(collection);
    coll.find({}).toArray((err,docs)=>{
        assert.strictEqual(err,null);
        console.log(docs);
        callback(docs);
    });
    
};


exports.removeDocument = (db,collection, callback) => {
    const coll=db.collection(collection);
    coll.deleteOne(document, (err,result)=>{
        assert.strictEqual(err,null);
        console.log("Removed the document");
        callback(result);
    });
};


exports.updateDocument = (db,document,update, collection, callback) => {
    const coll=db.collection(collection);
    console.log("Updating " , document );
    coll.updateOne(document, {$set:update}, null, (err,result)=>{
        assert.strictEqual(err,null);
        console.log('Updated ' , update);
        callback(result);
    });
};