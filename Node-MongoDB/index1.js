/*---------------------------THIS IS FOR CALLBACK HELL AND PROMISES JS-----------------*/

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017/';
const dbname = 'dishes';
const dbop = require('./operations1');

var value = 55;
console.log(value);

MongoClient.connect(url).then((client)=>{

    //assert.strictEqual(err,null);
    console.log('--connected to server--');
    const db =client.db(dbname);

    dbop.insertDocument(db, { "name": "Dal1", description:"DAL MAKHANI1"},'dishes')
    .then((result)=>{
        console.log('--Insterted Document--' + result.ops);
        console.log('Now Running Find command\n');

        return dbop.findDocuments(db,'dishes')
    })
    .then((docs)=> {
        console.log('\n-- Found Documents--',docs);

        return dbop.updateDocument(db,{name:'Dal1'},{description : 'Updated Test for Dal makhani1'},'dishes')
    })     
    .then((result)=>{
            console.log("\n--UPDATED--\n", result.result);

            return dbop.findDocuments(db,'dishes')
    })
    .then((docs)=>{
        console.log('--Found Documents 2nd time--',docs);
        client.close();
    })
    .catch((err)=>{
        console.log('catch 1\n');
        console.log(err);
    });
            
    })
    .catch((err)=>{
        console.log('catch 2\n')
        console.log(err)
    });
