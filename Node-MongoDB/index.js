const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017/';
const dbname = 'dishes';
const dbop = require('./operations');

var value = 55;
console.log(value);

MongoClient.connect(url,(err,client)=>{

    assert.strictEqual(err,null);
    console.log('--connected to server--');
    const db =client.db(dbname);

    dbop.insertDocument(db, { name: "Rice11", description:"India Gate11"},'dishes', (result)=>{
        console.log('--Insterted Document--' + result.ops);
        
        console.log('Now Running Find command\n');
        dbop.findDocuments(db,'dishes', ()=> {
            console.log('\n-- Found Documents--');

            dbop.updateDocument(db,{name:'Rice11'},{description : 'Updated Test for pizza123'},'dishes',(result)=>{
                console.log("\n--UPDATED--\n", result.result);

                dbop.findDocuments(db,'dishes', ()=>{
                    console.log('--Found Documents 2nd time--');

                    client.close();
                });
            });
        });
    });
});