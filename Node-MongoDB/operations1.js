/*---------------------------THIS IS FOR CALLBACK HELL AND PROMISES----------------------------*/

const assert = require('assert');


exports.insertDocument = (db,document,collection, callback) => {
    const coll=db.collection(collection);
    return coll.insertOne(document);

};


exports.findDocuments = (db,collection, callback) => {
    const coll=db.collection(collection);
    return coll.find({}).toArray();
    
};


exports.removeDocument = (db,collection, callback) => {
    const coll=db.collection(collection);
    return coll.deleteOne(document);
};


exports.updateDocument = (db,document,update, collection, callback) => {
    const coll=db.collection(collection);
    console.log("Updating " , document );
    return coll.updateOne(document, {$set:update}, null);

};